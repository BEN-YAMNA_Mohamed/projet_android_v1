package com.beny.ceri_musuem;

import java.util.Arrays;

public class items {
    String id;
    String name="";
    String[] categories;
    String description="";
    int[] timeFrame;
    int year=0;
    String brand="";
    String[] technicalDetails;
    Boolean working;
    String thumbnail="";
    String[] pictures;

    public items(String id, String name, String[] categorie, String description, int[] timeFrame,
                 int year, String brand, String[] technicalDetails, Boolean working, String thumbnail, String[] full_image) {
        this.id = id;
        this.name = name;
        this.categories = categorie;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.thumbnail = thumbnail;
        this.pictures = full_image;
    }

    public items(String id) {
        this.id = id;
        this.categories = new String[7];
        this.timeFrame = new int[3];
        this.technicalDetails = new String[10];
        this.pictures = new String[5];
    }

    @Override
    public String toString() {
        return "items{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", categories=" + Arrays.toString(categories) +
                ", description='" + description + '\'' +
                ", timeFrame=" + Arrays.toString(timeFrame) +
                ", year=" + year +
                ", brand='" + brand + '\'' +
                ", technicalDetails=" + Arrays.toString(technicalDetails) +
                ", working=" + working +
                ", thumbnail='" + thumbnail + '\'' +
                ", pictures=" + Arrays.toString(pictures) +
                '}';
    }
}
