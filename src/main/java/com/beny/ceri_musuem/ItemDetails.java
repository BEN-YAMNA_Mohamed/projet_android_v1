package com.beny.ceri_musuem;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

public class ItemDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        /*Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        Intent intent = getIntent();
        String temp="";
        temp = intent.getStringExtra("item_name");
        int index = MyViewHolder.getIdByName(temp);
        items x = APIparser.items[index];
        ImageView imageView = findViewById(R.id.imageView);
        TextView textView1 = findViewById(R.id.textView1);
        textView1.setText(x.name);
        TextView textView2 = findViewById(R.id.textView2);
        textView2.setText(x.description);
        TextView textView3 = findViewById(R.id.textView3);
        textView3.setText(Integer.toString(x.year));
        TextView textView4 = findViewById(R.id.textView4);
        textView4.setText(Arrays.toString(x.categories));
        TextView textView5 = findViewById(R.id.textView5);
        textView5.setText(x.brand);
        TextView textView6 = findViewById(R.id.textView6);
        textView6.setText(Arrays.toString(x.technicalDetails));
        TextView textView7 = findViewById(R.id.textView7);
        textView7.setText(Boolean.toString(x.working));
        TextView textView8 = findViewById(R.id.textView8);
        textView8.setText(Integer.toString(x.year));


    }

}
