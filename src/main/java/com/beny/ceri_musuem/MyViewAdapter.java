package com.beny.ceri_musuem;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MyViewAdapter extends RecyclerView.Adapter<MyViewHolder> implements Filterable {
    Context c;
    items[] item ;
    items[] item_copy;


    public MyViewAdapter (Context cc , items[] item){
        this.c=cc;
        this.item=item;
        this.item_copy = Arrays.copyOf(item,this.item.length);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(c).inflate(R.layout.model, viewGroup,false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        if(item[i]!=null) {
            final String name = item[i].name;
            myViewHolder.itemName.setText(name);
            String thumbnailURLStr = item[i].thumbnail;
            if(thumbnailURLStr!=null && thumbnailURLStr!="") {
                Picasso.get().load(thumbnailURLStr).fetch();
                Picasso
                        .get()
                        .load(thumbnailURLStr)
                        .placeholder(R.drawable.ic_loading)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(myViewHolder.itemPic);

                //Picasso.get().load(thumbnailURLStr).resize(50, 50).into(myViewHolder.itemPic);
            }
            myViewHolder.itemName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(c, ItemDetails.class);
                    intent.putExtra("item_name", name);
                    c.startActivity(intent);
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return item.length;
    }

    @Override
    public Filter getFilter() {
        return filter_items;
    }

    private Filter filter_items = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            items[] temp_filter = new items[item.length];

            if(charSequence == null || charSequence.length()==0){
                temp_filter = item_copy;
            }
            else{
                String filterPattern = charSequence.toString().toLowerCase().trim();
                int j=0;
                for(int i=0 ; i<item.length && item_copy[i]!=null;i++){
                    if(item_copy[i].name.toLowerCase().contains(filterPattern)){
                        temp_filter[j]=item_copy[i];
                        j++;
                    }
                }
            }
            FilterResults results = new FilterResults();
            List<items> temp_list = new ArrayList<items>(); //to remove null variables

            for(items s : temp_filter) {
                if(s != null && s.name.length() > 0) {
                    temp_list.add(s);
                }
            }

            temp_filter = temp_list.toArray(new items[temp_list.size()]);
            results.values = temp_filter;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            item = new items[50];
/*            List<items> temp_list = new ArrayList<items>();

            for(items s : item) {
                if(s != null && s.name.length() > 0) {
                    temp_list.add(s);
                }
            }


            item = temp_list.toArray(new items[temp_list.size()]);*/
            item = (items[]) filterResults.values;
            notifyDataSetChanged();

        }
    };
}
