package com.beny.ceri_musuem;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView itemName ;
    ImageView itemPic;
    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        itemName = (TextView) itemView.findViewById(R.id.itemName);
        itemPic = (ImageView) itemView.findViewById(R.id.itemPic);


    }

    static public int getIdByName(String name){
        for (int i=0; i<APIparser.items.length;i++){
            if (APIparser.items[i].name == name){
                return i;
            }
        }
        return 0;
    }
}
