package com.beny.ceri_musuem;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.CollationElementIterator;
import java.util.Arrays;
import java.util.Iterator;

public class APIparser extends AsyncTask  {

    Context c;

    static items[] items;

    /*public APIparser() throws IOException, JSONException {

        JSONObject json_ids = JsonReader.readJsonFromUrl("https://demo-lia.univ-avignon.fr/cerimuseum/ids");
        Iterator x = json_ids.keys();
        int j=0;
        while (x.hasNext()) {

            String id_temp = (String) x.next();
            items item_temp = new items(id_temp);
            remplisseur(item_temp);
            items[j] = item_temp;

        }
    }*/

    public APIparser(Context c) {
        items = new items[50];
        this.c = c;
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected Object doInBackground(Object[] objects) {

        JSONArray json_ids_array =null;
        try {
            json_ids_array = new JSONArray(URLSourceScraper.getURLSource("https://demo-lia.univ-avignon.fr/cerimuseum/ids"));
            Log.i("items ids",json_ids_array.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int i=0 ; i< json_ids_array.length(); i++){
            String id_temp = null;
            try {
                id_temp = json_ids_array.getString(i);
                //Log.i("item "+i, id_temp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            items item_temp = new items(id_temp);
            try {
                item_temp = remplisseur(item_temp);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            items[i] = item_temp;
        }

        return Arrays.toString(items);
    }


    static items remplisseur(items i) throws IOException, JSONException {
        JSONObject json_item = JsonReader.readJsonFromUrl("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+i.id);

        i.name = json_item.getString("name");
        JSONArray categorie = json_item.getJSONArray("categories");
        for (int h = 0; h < categorie.length(); ++h) {
            i.categories[h] = categorie.optString(h);
        }
        i.description = json_item.getString("description");
        JSONArray timeFrame = json_item.getJSONArray("timeFrame");
        for (int h = 0; h < timeFrame.length(); ++h) {
            i.timeFrame[h] = timeFrame.optInt(h);
        }
        i.year = json_item.getInt("year");
        i.brand = json_item.getString("name");;
        JSONArray techDetails = json_item.getJSONArray("technicalDetails");
        for (int h = 0; h < techDetails.length(); ++h) {
            i.technicalDetails[h] = techDetails.optString(h);
        }
        i.working = json_item.getBoolean("working");
        i.thumbnail = "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+i.id+"/thumbnail";


        JSONObject keys = json_item.getJSONObject("pictures");
        Iterator ikeys = keys.keys();
        while(ikeys.hasNext()) {
            int h=0;
            // chaque id d'image et suivi par sa description
            String keyName = (String)ikeys.next();
            String keyDescription = json_item.getString(keyName);
            i.pictures[h]= "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+i.id+"/images/"+keyName;
            h++;
            i.pictures[h]= keyDescription;
            h++;
        }

        return i;
    }

    static MyViewAdapter myAdapter;
    @Override
    protected void onPostExecute(Object o) {
        //TextView mainText;
        //mainText = (TextView) ((Activity) c).findViewById(R.id.mainText);
        //mainText.setText(Arrays.toString(this.items));
        TextView loading = ((Activity) c).findViewById(R.id.loading);
        loading.setVisibility(View.GONE);
        RecyclerView rv = (RecyclerView) ((Activity) c).findViewById(R.id.recycleview);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(c));
        myAdapter = new MyViewAdapter(c,APIparser.items);
        rv.setAdapter(myAdapter);

    }

    @Override
    public String toString() {
        return Arrays.toString(this.items);
    }
}
